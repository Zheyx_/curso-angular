import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FnParam } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-vuelosdetallecomponent',
  templateUrl: './vuelosdetallecomponent.component.html',
  styleUrls: ['./vuelosdetallecomponent.component.css']
})
export class VuelosdetallecomponentComponent implements OnInit {
  id:any
  constructor(private route: ActivatedRoute) {
    route.params.subscribe(params =>{this.id = params['id'];});
   }

  ngOnInit(): void {
  }

}
