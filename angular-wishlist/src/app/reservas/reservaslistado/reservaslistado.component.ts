import { Component, OnInit } from '@angular/core';
import { ReservasapiclientService } from '../reservasapiclient.service';

@Component({
  selector: 'app-reservaslistado',
  templateUrl: './reservaslistado.component.html',
  styleUrls: ['./reservaslistado.component.css']
})
export class ReservaslistadoComponent implements OnInit {

  constructor(private api: ReservasapiclientService) { }

  ngOnInit(): void {
  }

}
