import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservaslistadoComponent } from "./reservaslistado/reservaslistado.component";
import { ReservasdetalleComponent } from "./reservasdetalle/reservasdetalle.component";

const routes: Routes = [
  {path: 'reservas', component: ReservaslistadoComponent},
  {path: 'reservas/:id', component: ReservasdetalleComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
