import express, { json } from "express";
var express= require("express"), cors = require('cors');
var app = express();
app.use(json());
app.use(cors());
app.listen(3000,()=> console.log("server running on port 3000"));
var ciudades = ["Paris","Barcelona","Barranquilla","Montevideo","Santiago"]
app.get("/ciudades", (req,res,next)=> res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLocaleLowerCase()) > -1)));

var misdestinos = [];
app.get("/my",(req,res,next)=> res.json(misdestinos));
app.post("/my",(req,res,next)=>{
    console.log(req.body);
    misdestinos.push(req.body.nuevo);
    res.json(misdestinos);
});

app.get("/api/translation", (req,res,next)=>res.json([
    {lang: req.query.lang,key:'HOLA', value: 'HOLA' + req.query.lang}
]));