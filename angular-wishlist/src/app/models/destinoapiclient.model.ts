import { destinoviaje} from './destinoviaje.model';
import { AppState, APP_CONFIG, AppConfig,db } from '../app.module';
import { Store } from '@ngrx/store';
import { nuevodestinoAction, elegidofavoritoAction } from './destinoviajestate.model';
import { Injectable, forwardRef, Inject } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaderResponse, HttpHeaders, HttpRequest, HttpResponse } from "@angular/common/http";

@Injectable()
export class destinoapiclient {
    destinos: destinoviaje[] = [];

    constructor(
        private store:Store<AppState>,
        @Inject(forwardRef(()=>APP_CONFIG))private config:AppConfig,private http: HttpClient){
        this.store.select(state=>state.destinos).subscribe((data)=>{
            console.log('destinos sub store');
            console.log(data);
            this.destinos=data.items;
        });
        this.store.subscribe((data)=>{
            console.log('all store');
            console.log(data);
        });
    }
    add(d:destinoviaje){
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint+ '/my', {nuevo:d.nombre}, {headers:headers});
        this.http.request(req).subscribe((data:HttpResponse<{}>)=>{
            if(data.status ===200){
                this.store.dispatch(new nuevodestinoAction(d));
                const myDb = db;
                myDb.destinos.add(d);
                console.log('todos los destinos de la db');
                myDb.destinos.toArray().then(destinos=>console.log(destinos))
            }
        });
    }
    
    getById(id:String) : destinoviaje{
        return this.destinos.filter(function(d){ return d.id.toString() === id;})[0];
    }

    getAll(): destinoviaje[]{
        return this.destinos;
    }
    
    elegir(d:destinoviaje){
        this.store.dispatch(new elegidofavoritoAction(d));
    }


}


