import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservaslistadoComponent } from './reservaslistado/reservaslistado.component';
import { ReservasdetalleComponent } from './reservasdetalle/reservasdetalle.component';
import { ReservasapiclientService } from "./reservasapiclient.service";

@NgModule({
  declarations: [ReservaslistadoComponent, ReservasdetalleComponent],
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],
  providers: [
    ReservasapiclientService
  ]
})
export class ReservasModule { }
