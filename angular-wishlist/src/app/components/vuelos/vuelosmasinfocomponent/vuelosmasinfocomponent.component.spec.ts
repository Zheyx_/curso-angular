import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuelosmasinfocomponentComponent } from './vuelosmasinfocomponent.component';

describe('VuelosmasinfocomponentComponent', () => {
  let component: VuelosmasinfocomponentComponent;
  let fixture: ComponentFixture<VuelosmasinfocomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuelosmasinfocomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuelosmasinfocomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
