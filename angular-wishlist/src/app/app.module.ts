import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, APP_INITIALIZER, Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Dexie } from "dexie";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NgxMapboxGLModule } from "ngx-mapbox-gl";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from './app.component';
import { DestinoviajeComponent } from './components/destinoviaje/destinoviaje.component';
import { ListadestinosComponent } from './components/listadestinos/listadestinos.component';
import { DestinodetalleComponent } from './components/destinodetalle/destinodetalle.component';
import { FormdestinoviajeComponent } from './components/formdestinoviaje/formdestinoviaje.component';
import { destinoviajestate, reducerdestinoviaje, intializedestinoviajestate, destinoviajeeffects, InitMyDataAction } from "./models/destinoviajestate.model";
import { ActionReducerMap, StoreModule as NgRxStoreModule, Store } from '@ngrx/store';
import { EffectsModule } from "@ngrx/effects";
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogeadoGuard } from "./guards/usuario-logeado/usuario-logeado.guard";
import { VueloscomponentComponent } from './components/vuelos/vueloscomponent/vueloscomponent.component';
import { VuelosmaincomponentComponent } from './components/vuelos/vuelosmaincomponent/vuelosmaincomponent.component';
import { VuelosmasinfocomponentComponent } from './components/vuelos/vuelosmasinfocomponent/vuelosmasinfocomponent.component';
import { VuelosdetallecomponentComponent } from './components/vuelos/vuelosdetallecomponent/vuelosdetallecomponent.component';
import { ReservasModule } from './reservas/reservas.module';
import { destinoviaje } from './models/destinoviaje.model';
import { Observable, from } from 'rxjs';
import { flatMap, elementAt } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

//app config
export interface AppConfig{
  apiEndpoint: String;
}

const APP_CONFIG_VALUE:AppConfig ={
  apiEndpoint:'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('appconfig');

//fin app config

//routing
export const childrenroutesvuelos:Routes = [
  {path:'', redirectTo:'main', pathMatch:'full'},
  {path:'main',component: VuelosmaincomponentComponent},
  {path:'mas-info', component:VuelosmasinfocomponentComponent},
  {path:':id', component: VuelosdetallecomponentComponent}
  ];



const routes: Routes = [
  {path: '',redirectTo: 'home', pathMatch:'full'},
  {path:'home', component: ListadestinosComponent},
  {path:'destino/:id', component: DestinodetalleComponent},
  {path: 'login', component:LoginComponent},
  {
    path: 'protected',
    component: ProtectedComponent, 
    canActivate: [UsuarioLogeadoGuard]

  },
  {path:'vuelos',
  component: VueloscomponentComponent,
  canActivate: [UsuarioLogeadoGuard],
  children:childrenroutesvuelos
  }
];

// end routing



//redux init
export interface AppState{
  destinos: destinoviajestate;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerdestinoviaje,
};

let reducersInitialState={
  destinos: intializedestinoviajestate()
};


//redux fin init

//app init

export function init_app(appLoadService:AppLoadService):()=>Promise<any>{
  return () => appLoadService.intializedestinoviajestate();
}
@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async intializedestinoviajestate():Promise<any>{
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint	+ '/my', {headers: headers});
    const response : any =await  this.http.request(req).toPromise	();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}


//end app init

//Dexie db

export class Translation {
  constructor(id: number,public lang:string, public key: string, public value: string){}
}

@Injectable({
  providedIn:'root'
})

export class MyDataBase extends Dexie {
  destinos: Dexie.Table<destinoviaje, number>;
  translations: any;
  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      destinos:'++id, nombre, imagenurl',
    });
    this.version(2).stores({
      destinos:'++id,nombre, imagenurl',
      translations: '++id, lang,key,value'
    });
  }
}

export const db = new MyDataBase();

//end Dexue db

//i18n ini
class TranslationLoader implements TranslateLoader{
  constructor(private http: HttpClient){}
  getTranslation(lang:string):Observable<any>{
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results=>{
                                      if (results.length === 0){
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang='+lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones)=> {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones)=>{
                                        return traducciones.map((t)=>({ [t.key]:t.value }));
                                      });
    return from(promise).pipe(flatMap((elems)=> from (elems)));
  }
}

function HttpLoaderFactory(http: HttpClientModule){
  return new TranslationLoader (http);
}

//end i18n

@NgModule({
  declarations: [
    AppComponent,
    DestinoviajeComponent,
    ListadestinosComponent,
    DestinodetalleComponent,
    FormdestinoviajeComponent,
    LoginComponent,
    ProtectedComponent,
    VueloscomponentComponent,
    VuelosmaincomponentComponent,
    VuelosmasinfocomponentComponent,
    VuelosdetallecomponentComponent,
    EspiameDirective,
    TrackearClickDirective,
     
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers,{initialState:reducersInitialState}),
    EffectsModule.forRoot([destinoviajeeffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader:{
        provide:TranslationLoader,
        useFactory:(HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService, UsuarioLogeadoGuard,
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    AppLoadService,
    {provide: APP_INITIALIZER, useFactory: init_app, deps:[AppLoadService], multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
