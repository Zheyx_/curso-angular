import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuelosmaincomponentComponent } from './vuelosmaincomponent.component';

describe('VuelosmaincomponentComponent', () => {
  let component: VuelosmaincomponentComponent;
  let fixture: ComponentFixture<VuelosmaincomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuelosmaincomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuelosmaincomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
