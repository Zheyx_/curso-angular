import { Component, OnInit } from '@angular/core';
import { destinoapiclient } from "./../../models/destinoapiclient.model";
import { destinoviaje } from "./../../models/destinoviaje.model";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-destinodetalle',
  templateUrl: './destinodetalle.component.html',
  styleUrls: ['./destinodetalle.component.css'],
  providers:[ destinoapiclient ]
})

export class DestinodetalleComponent implements OnInit {
  destino:destinoviaje;
  style= {
    sources: {
      world:{
        type: 'geojson',
        data: 'http://raw.githubusercontent.com/johan/world.geo.json/master/countries'
      }
    },
    version:8,
    layers:[{
      'id:':'countries',
      'type':'fill',
      'source': 'world',
      'layout': {},
      'paint' : {
        'fill-color':'#6F788A'
      }
    }]
  };

  constructor(private route: ActivatedRoute, private destinoapiclient:destinoapiclient) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinoapiclient.getById(id);
  }
 
}


 