import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { destinoviaje } from './../../models/destinoviaje.model';
import { AppState } from './../../app.module';
import { voteupAction, votedownAction } from './../../models/destinoviajestate.model';
import { Store } from '@ngrx/store';
import { trigger, state, style, transition, animate } from "@angular/animations";

@Component({
  selector: 'app-destinoviaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.css'],
  animations:[
    trigger('esFavorito',[
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito=>estadoFavorito',
      [animate('3s')]),
      transition('estadoFavorito=>estadoNoFavorito',
      [animate('1s')]),   
    ])]
})
export class DestinoviajeComponent implements OnInit {
  @Input () destino: destinoviaje;
  @Input ("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<destinoviaje>;


  constructor(private store:Store<AppState>) {
    this.onClicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.onClicked.emit(this.destino);
    return false;
  }

  voteup(){
    this.store.dispatch(new voteupAction(this.destino));
    return false;
  }

  votedown(){
    this.store.dispatch(new votedownAction(this.destino));
    return false;
  }

}
