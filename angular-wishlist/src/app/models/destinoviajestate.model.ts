import { Injectable} from '@angular/core';
import { Action } from "@ngrx/store";
import { Actions,Effect,ofType, act } from "@ngrx/effects";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { destinoviaje } from "./destinoviaje.model";
import { HttpClientModule } from "@angular/common/http";

//estado
export interface destinoviajestate{
    items: destinoviaje[];
    loading: boolean;
    favorito: destinoviaje;
}

export function intializedestinoviajestate(){
    return{
        items: [],
        loading: false,
        favorito:null
    };
}

//acciones
export enum destinoviajeActionTypes{
    NUEVO_DESTINO = '[destino viaje] nuevo',
    ELEGIDO_FAVORITO = '[destino viaje] favorito',
    VOTE_UP = '[destino viaje] vote up',
    VOTE_DOWN = '[destino viaje] vote down',
    INIT_MY_DATA = '[destino viaje] Init My Data',
}

export class nuevodestinoAction implements Action{
    type = destinoviajeActionTypes.NUEVO_DESTINO;
    constructor(public destino:destinoviaje){}
}

export class elegidofavoritoAction implements Action{
    type = destinoviajeActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino:destinoviaje){}
}

export class voteupAction implements Action{
    type = destinoviajeActionTypes.VOTE_UP;
    constructor(public destino:destinoviaje){}
}

export class votedownAction implements Action{
    type = destinoviajeActionTypes.VOTE_DOWN;
    constructor(public destino:destinoviaje){}
}

export class InitMyDataAction implements Action{
    type = destinoviajeActionTypes.INIT_MY_DATA;
    constructor(public destino:destinoviaje){}
}

export type destinoviajeActions = nuevodestinoAction | elegidofavoritoAction | voteupAction | votedownAction | InitMyDataAction;

//reducers
export function reducerdestinoviaje (
    state: destinoviajestate,
    action: destinoviajeActions
    ):destinoviajestate{
        switch(action.type){

            case destinoviajeActionTypes.INIT_MY_DATA:{
                const destino: string[] = (action as InitMyDataAction).destino;
                return {
                    ...state,
                    items: destino.map((d) => new destinoviaje (d,''))
                };

            }           
            case destinoviajeActionTypes.NUEVO_DESTINO:{
                return{...state,items:[...state.items,(action as nuevodestinoAction).destino]};
            }
            case destinoviajeActionTypes.ELEGIDO_FAVORITO:{
                state.items.forEach(x => x.setSelected(false));
                const fav: destinoviaje = (action as elegidofavoritoAction).destino;
                fav.setSelected(true);
                return{...state,favorito:fav};
            }
        case destinoviajeActionTypes.VOTE_UP:{
            const d: destinoviaje = (action as voteupAction).destino;
            d.voteup;
            return{...state};
        }
        case destinoviajeActionTypes.VOTE_DOWN:{
            const d: destinoviaje = (action as votedownAction).destino;
            d.votedown;
            return{...state};
        }
    }
    return state;
}
    
//effects
@Injectable()
export class destinoviajeeffects{
    @Effect()
    nuevoagregado$:Observable<Action> = this.actions$.pipe(
        ofType(destinoviajeActionTypes.NUEVO_DESTINO),
        map((action:nuevodestinoAction)=>new elegidofavoritoAction(action.destino))
    );
   constructor(private actions$:Actions){}
    
}











