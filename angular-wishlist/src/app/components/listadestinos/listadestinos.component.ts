import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { destinoviaje} from './../../models/destinoviaje.model';
import { destinoapiclient} from './../../models/destinoapiclient.model';
import { AppState } from './../../app.module';
import { Store } from "@ngrx/store";
import { nuevodestinoAction, elegidofavoritoAction } from './../../models/destinoviajestate.model';

@Component({
  selector: 'app-listadestinos',
  templateUrl: './listadestinos.component.html',
  styleUrls: ['./listadestinos.component.css'],
  providers:[ destinoapiclient]
})
export class ListadestinosComponent implements OnInit {
  @Output () onItemAdded: EventEmitter<destinoviaje>;
  updates: string[];
  all;
  
  constructor(private destinoapiclient:destinoapiclient, private store:Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state=>state.destinos.favorito)
    .subscribe(d=>{
      if (d !=null){
        this.updates.push('se ha elegido a' + d.nombre);
      } 
    });
    store.select(state=>state.destinos.items).subscribe(items=>this.all=items);
   }

  ngOnInit(): void {
  }

  agregado(d: destinoviaje){
    this.destinoapiclient.add(d);
    this.onItemAdded.emit(d);
  }
  

  elegido(e: destinoviaje){
    this.destinoapiclient.elegir(e);
  }

  getAll(){

  }
}
