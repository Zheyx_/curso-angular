export class destinoviaje {
    private selected: boolean;
    public servicios: string[];
    id: any;
    constructor(public nombre:string,public imagenurl:string, public votes:number=0){
        this.servicios = ['Zona húmeda','Desayuno y cena']; 
    }

    isSelected(): boolean{
        return this.selected;
    }
    setSelected(s:boolean){
        this.selected = s;
    }

    voteup(){
        this.votes++;
    }
    votedown(){
        this.votes--;
    }
}