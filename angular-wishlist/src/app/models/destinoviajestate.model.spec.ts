import { reducerdestinoviaje,destinoviajestate,intializedestinoviajestate,InitMyDataAction,nuevodestinoAction } from './destinoviajestate.model';
import { destinoviaje } from "./destinoviaje.model";
import { act } from '@ngrx/effects';

describe('reducerdestinoviaje',()=>{
    it('should reduce init data',()=>{
        const prevState:destinoviajestate = intializedestinoviajestate();
        const action: InitMyDataAction = new InitMyDataAction(['destino1','destino2']);
        const newState: destinoviajestate = reducerdestinoviaje(prevState,action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino1');
    });
    it('should reduce init data',()=>{
        const prevState:destinoviajestate = intializedestinoviajestate();
        const action: InitMyDataAction = new InitMyDataAction(['barcelona','url']);
        const newState: destinoviajestate = reducerdestinoviaje(prevState,action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });
});