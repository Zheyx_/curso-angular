import { Component, OnInit, Output, EventEmitter, Injectable, forwardRef, Inject } from '@angular/core';
import { destinoviaje } from './../../models/destinoviaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter,debounceTime,distinctUntilChanged,switchMap} from 'rxjs/operators';
import { ajax, AjaxResponse} from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';


@Component({
  selector: 'app-formdestinoviaje',
  templateUrl: './formdestinoviaje.component.html',
  styleUrls: ['./formdestinoviaje.component.css']
})
export class FormdestinoviajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<destinoviaje>;
  fg: FormGroup;
  minlongitud = 3;
  searchresults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(()=>APP_CONFIG)) private config: AppConfig ) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre:['',Validators.compose([
        Validators.required,
        this.nombrevalidatorparametrizable(this.minlongitud)
      ])],
      url:['']
    });

    this.fg.valueChanges.subscribe((form:any)=>{
      console.log('cambio el formulario:');
    });

   }

  ngOnInit(): void {
    let elemnombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemnombre,'input')
    .pipe(
      map((e:KeyboardEvent)=>(e.target as HTMLInputElement).value),
      filter(text => text.length >2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text:string)=> ajax(this.config.apiEndpoint + '/ciudades?q='+text))
      ).subscribe(AjaxResponse => {this.searchresults = AjaxResponse.response;
      });
  }

  guardar(nombre:string,url:string):boolean{
    const d = new destinoviaje(nombre,url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombrevalidator(control:FormControl):{[s:string]:boolean}{
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5){
      return{invalidnombre:true};
    }
    return null;
  }

  nombrevalidatorparametrizable(minlong:number):ValidatorFn{
    return(control:FormControl):{[s:string]:boolean} | null => {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < minlong){
      return{minlongnombre:true};
    }
      return null;
    }
  }


}
